﻿/*
 * Created by SharpDevelop.
 * User: VolodyaM
 * Date: 10.06.2015
 * Time: 0:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MoNetVoiceControl
{
	public class CSCoreSpeech
	{
		public string text = "";
		public int textOffset = 0;
		public int maxPartLength = 100;
		public CSCore.SoundOut.ISoundOut soundOut = null;
		public YandexSpeech speech = null;
		
		private bool needToPlay = false;
		
		public void PreparePlay() {
			needToPlay = true;
		}
		
		public bool WaitPlay() {
			if(needToPlay) {
				needToPlay = false;
				if(textOffset != text.Length) {
					int length = System.Math.Min(maxPartLength, text.Length - textOffset);
					
					//Find first whitespace before
					int backward = 0;
					while(text[textOffset + length + backward] != ' ' && (length + backward) != 0) {
						backward--;
					}
					
					//Not enought length, get more
					if(length + backward == 0) {
						backward = 0;
						while((text[textOffset + length] != ' ') && (textOffset+length < text.Length)) {
							length++;
						}
					} else {
						length += backward;
					}
					
					string toRead = text.Substring(textOffset, length);
					textOffset += length;
					speech.text = toRead;
					
					var str = new CSCore.Codecs.WAV.WaveFileReader(speech.BufferRequest());
					soundOut.Initialize(str);
					soundOut.Play();
				}
			}
			return textOffset <= text.Length;
		}
		
		private void _OnStopped(object sender, CSCore.SoundOut.PlaybackStoppedEventArgs e) {
			if(!e.HasError)
				needToPlay = true;
		}
		
		public CSCoreSpeech(string Text, CSCore.SoundOut.ISoundOut SoundOut, YandexSpeech Speech)
		{
			soundOut = SoundOut;
			speech = Speech;
			text = Text;
			
			SoundOut.Stopped += _OnStopped;
		}
		
		~CSCoreSpeech() {
			soundOut.Stopped -= _OnStopped;
		}
		
		public void SaveConfig(string fileName) {
			using(var textWriter = System.IO.File.CreateText(fileName)) {
				textWriter.WriteLine("text="+text);
				textWriter.WriteLine("speaker="+speech.speaker);
				textWriter.WriteLine("emotion="+speech.emotion);
				textWriter.WriteLine("drunk="+speech.drunk);
				textWriter.WriteLine("ill="+speech.ill);
				textWriter.WriteLine("robot="+speech.robot);
				textWriter.WriteLine("maxPartLength="+maxPartLength);
			}
		}
		
		public void LoadConfig(string fileName) {
			var cfg = new System.Collections.Generic.Dictionary<string, string>();
			using(var reader = System.IO.File.OpenText(fileName)) {
				var textReader = (reader as System.IO.TextReader);
				string line = "";
				while(line != null) {
					if(line != "") cfg.Add(line.Substring(0, line.IndexOf('=')), line.Substring(line.IndexOf('=')+1));
					line = textReader.ReadLine();
				}
			}
			text = cfg["text"];
			speech.speaker = (YandexSpeech.Speaker)Enum.Parse(typeof(YandexSpeech.Speaker), cfg["speaker"]);
			speech.emotion = (YandexSpeech.Emotion)Enum.Parse(typeof(YandexSpeech.Emotion), cfg["emotion"]);
			speech.drunk = bool.Parse(cfg["drunk"]);
			speech.ill = bool.Parse(cfg["ill"]);
			speech.robot = bool.Parse(cfg["robot"]);
			maxPartLength = int.Parse(cfg["maxPartLength"]);
			
			if(cfg.ContainsKey("textfile")) {
				text = System.IO.File.ReadAllText(cfg["textfile"], System.Text.Encoding.GetEncoding(int.Parse(cfg["textfilecodepage"])));
			}
		}
	}
}
