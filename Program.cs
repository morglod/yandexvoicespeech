﻿/*
 * Created by SharpDevelop.
 * User: VolodyaM
 * Date: 08.06.2015
 * Time: 21:34
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MoNetVoiceControl
{
	class Program
	{
		public static CSCore.SoundOut.ISoundOut NeedOutput() {
			if(CSCore.SoundOut.WasapiOut.IsSupportedOnCurrentPlatform) {
				return new CSCore.SoundOut.WasapiOut();
			} else {
				return new CSCore.SoundOut.WaveOut();
			}
		}
		
		public static CSCore.SoundIn.ISoundIn NeedInput() {
			var s = new CSCore.SoundIn.WasapiCapture();
			s.Initialize();
			return s;
		}
		
		public static void Main(string[] args)
		{
			//Request CSCore sound output
			var output = NeedOutput();
			
			YandexSpeech yaspeech = new YandexSpeech();
			CSCoreSpeech speech = new CSCoreSpeech("", output, yaspeech);
			
			speech.LoadConfig("config.cfg");
			
			speech.PreparePlay();
			while(speech.WaitPlay()) { System.Threading.Thread.Sleep(100); }
			
			System.Threading.Thread.Sleep(1000);
			
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}