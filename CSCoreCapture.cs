﻿/*
 * Created by SharpDevelop.
 * User: VolodyaM
 * Date: 18.06.2015
 * Time: 14:46
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MoNetVoiceControl
{
	/// <summary>
	/// Description of CSCoreCapture.
	/// </summary>
	public class CSCoreCapture
	{
		public readonly CSCore.SoundIn.ISoundIn soundIn = null;
		
		public class Record {
			public readonly System.IO.MemoryStream memory = new System.IO.MemoryStream();
			public readonly CSCore.Codecs.WAV.WaveWriter writer = null;
			public readonly CSCore.SoundIn.ISoundIn soundIn = null;
			
			public byte[] Stop() {
				soundIn.Stop();
				return memory.ToArray();
			}
			
			void OnData(object sender, CSCore.SoundIn.DataAvailableEventArgs e) {
				writer.Write(e.Data, e.Offset, e.ByteCount);
			}
			
			public Record(CSCore.SoundIn.ISoundIn soundIn) {
				this.soundIn = soundIn;
				writer = new CSCore.Codecs.WAV.WaveWriter(mem, soundIN.WaveFormat);
				soundIn.DataAvailable += OnData;
				soundIn.Start();
			}
			
			~Record() {
				soundIn.DataAvailable -= OnData;
			}
		}
		
		public Record Start() {
			return new Record(soundIn);
		}
		
		public CSCoreCapture(CSCore.SoundIn.ISoundIn soundIn)
		{
			this.soundIn = soundIn;
		}
	}
}
