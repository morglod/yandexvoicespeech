﻿/*
 * Created by SharpDevelop.
 * User: VolodyaM
 * Date: 09.06.2015
 * Time: 23:20
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MoNetVoiceControl
{
	/// <summary>
	/// Using Yandex Speech Api
	/// </summary>
	public class YandexSpeech
	{
		public enum AudioFormat {
			//mp3,
			wav
		}
		
		public enum Speaker {
			//woman
			jane,
			omazh,
			
			//man
			zahar,
			ermil
		}
		
		public enum Emotion {
			good,
			neutral,
			evil,
			mixed
		}
		
		public string text = "";
		public AudioFormat audioFormat = AudioFormat.wav;
		public Speaker speaker = Speaker.zahar;
		public Emotion emotion = Emotion.neutral;
		public bool drunk = false;
		public bool ill = false;
		public bool robot = false;
		
		public string QueryString {
			get {
				const string queryStr = "{0}?text=\"{1}\"&format={2}&lang={3}&speaker={4}&key={5}&emotion={6}&drunk={7}&ill={8}&robot={9}";
				const string langStr = "ru-RU";
				return string.Format(queryStr, YandexSpeechAPI.Api, text, audioFormat.ToString(), langStr, speaker.ToString(), YandexSpeechAPI.ApiKey, emotion.ToString(), drunk.ToString(), ill.ToString(), robot.ToString());
			}
		}
		
		public System.Net.WebResponse MakeWebRequest() {
			var req = System.Net.WebRequest.Create(QueryString);
			return req.GetResponse();
		}
		
		public System.IO.MemoryStream BufferRequest() {
			var webResponse = MakeWebRequest();
			
			var memStream = new System.IO.MemoryStream(1024);
			var responseStream = webResponse.GetResponseStream();
			int readed = 1;
			while(responseStream.CanRead && readed > 0) {
				var buf = new byte[1024];
				readed = responseStream.Read(buf, 0, 1024);
				memStream.Write(buf, 0, readed);
			}
			memStream.Position = 0;
			
			return memStream;
		}
	}
	
	public abstract class YandexSpeechAPI
	{
		public static string ApiKey = "e6234e1d-043f-4453-811f-b4b60732116e";
		public const string Api = "https://tts.voicetech.yandex.net/generate";
	}
}
